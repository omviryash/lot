<?php
session_start();
date_default_timezone_set("Asia/Kolkata");
error_reporting(E_ERROR | E_WARNING | E_PARSE | E_NOTICE);

//get the current script name
$script_name = explode('/', $_SERVER['PHP_SELF']);
$script_name = end($script_name);

$resaller_access_pages = array(
                            'index.php', 
                            'dashboard.php', 
                            'logout.php', 
                            'ajax.php',
                            'ticket.php',
                            'purchase.php',
                            'luckydraw.php',
                            'post.php',
                            'winner.php'
                            );
if(isset($_SESSION['usertype']) && $_SESSION['usertype'] == 1 && !in_array($script_name, $resaller_access_pages))
{
  header("Location: dashboard.php");
}
if(!isset($_SESSION['usertype']) && $script_name != 'index.php')
  header("Location: index.php");
  
define("ADMIN_IP_RESTRICTION", FALSE);
define("USER_IP_RESTRICTION", FALSE);  
  
$host = "localhost"; //database location
$user = "root"; //database username
$pass = ""; //database password

//$user = "root"; //database username
//$pass = ""; //database password

$dbName = "lot"; //database name

$link = mysql_connect($host, $user, $pass);//database connection
mysql_select_db($dbName);

//$baseUrl="http://www.gujaratgoldvapi.com/";
$baseUrl="http://localhost/lot/";

$imgPath="images/";

$yantra = array("Kartik Yantra","Gajanand Yantra","Gangotri Yantra","Bajrang Yantra","Bhairav Yantra","Ridhi Yantra","Sava Yantra","Suraj Yantra","Sidhi Yantra","Vastu Yantra");

$package = 1;
$company_id = 1;
if($package == 2) $cssLoad = "main"; else $cssLoad = "mainp1";
if($package == 2) $imageLoad = ""; else $imageLoad = "p1/";

if($company_id == 2) $cssLoad = "main"; else $cssLoad = "mainp1";
if($company_id == 2) $imageLoad = ""; else $imageLoad = "p1/";

function formatAmt($amt){
	if(!is_numeric($amt)) $amt = 0;
	return number_format($amt, 2, '.', '');
}

function generateRandomString($length = 8) {
  $characters = '0123456789';
  $randomString = '';
  for ($i = 0; $i < $length; $i++) {
      $randomString .= $characters[rand(0, strlen($characters) - 1)];
  }
  if(mysql_num_rows(mysql_query("SELECT hash_key FROM receipt_master WHERE hash_key = '".$randomString."'"))<=0)
  	return $randomString;
  else
  	generateRandomString();
}

function rtnretailer($id){
	if(is_numeric($id)){ 
		$sSQL1 = "SELECT username FROM users WHERE user_id = ".$id;
		$rs1 = mysql_query($sSQL1) or print(mysql_error());
		if(mysql_num_rows($rs1) > 0){
			$row1 = mysql_fetch_array($rs1);
			return ucfirst($row1["username"]);
		}
	}
}
?>