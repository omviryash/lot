<?php
include_once('includes/basepath.php');if(!isset($_SESSION['username'])){	header('Location: index.php');}
?>
<html>
<head>
	<link rel="stylesheet" type="text/css" href="css/<?php echo $cssLoad;?>.css" />
	<link rel="stylesheet" type="text/css" href="css/custom.css" />
	 <link href="css/datatables/dataTables.bootstrap.css" rel="stylesheet" type="text/css" />
	<script type="text/javascript" src="js/jquery-1.9.1.min.js"></script>
	<script type="text/javascript" src="js/cycle.js"></script>
	<script type="text/javascript" src="js/cycle.tile.js"></script>
	<!-- DATA TABES SCRIPT -->
	<script src="js/plugins/datatables/jquery.dataTables.js" type="text/javascript"></script>
	<script src="js/plugins/datatables/dataTables.bootstrap.js" type="text/javascript"></script>	
</head>

<body>
	<?php include_once('menu.php');?>
	<br />
	<br />
	<form name="frm" action="report_new.php" method="post">
		Select Retailer : <select name="user_id" onchange="frm.submit();">
			<option value="">All</option>
			<?php
			$sSQL = "SELECT user_id,username FROM users WHERE usertype = 1 ORDER BY username";
			$rs = mysql_query($sSQL);
			if(mysql_num_rows($rs) > 0){
				while($row = mysql_fetch_array($rs)){
					if(isset($_POST["user_id"]) && $row["user_id"] == $_POST["user_id"]) $a = ' selected'; else $a = '';
					echo '<option value="'.$row["user_id"].'" '.$a.'>'.$row["username"].'</option>';
				}
			}
			?>
		</select>
	</form>	
	
	<div id="mainWrapper" style="margin-top:20px;">
		<div class="box-body table-responsive">
				<table id="example1" cellpadding="4" cellspacing="0" border="1" class="table table-bordered table-striped">
					<thead>
						<tr>
						   <th rowspan="2">Draw Id</th>
							<th rowspan="2">Draw DateTime</th>
							<th colspan="11">Product</th>                                               
							<th colspan="3">&nbsp;</th>
						</tr>
						<tr>                                                
							<th>1</th><th>2</th>
							<th>3</th><th>4</th>
							<th>5</th><th>6</th>
							<th>7</th><th>8</th>
							<th>9</th><th>10</th>
							<th>Total</th>
							<th>Win</th>
							<th>&nbsp;</th>
							<th>Percent</th>
						</tr>
					</thead>
					<tbody>                  
					<?php
					for($i=0;$i<10;$i++)
					{
						$products[$i] = $i+1; 
					}
					$m=0;
					$qry = "SELECT * FROM draw
					        WHERE drawdatetime LIKE '".date("Y-m-d")."%'
					        ORDER BY drawdatetime";
					$res = mysql_query($qry) or print(mysql_error());											
					$nums = mysql_num_rows($res);
					while($row = mysql_fetch_array($res))
					{												
						$totalProducts = 0;
						if($nums != 0)
						{
							$drawid = $row['draw_id'];
							echo "<tr><td>".$drawid."</td>";
							$time = date("d-m-Y h:i:s",strtotime($row['drawdatetime']));										
							echo "<td>".$time."</td>";
							$qry2 = "SELECT * FROM receipt_master where draw_id='$drawid' AND receipt_cancel = 0 ";
							if(isset($_POST["user_id"]) && trim($_POST["user_id"]) != ""){
								$qry2 = $qry2 . " AND retailer_id = ".trim($_POST["user_id"]);
							}
							$res2 = mysql_query($qry2) or print(mysql_error());
							$num_rows = mysql_num_rows($res2);
							if($num_rows != 0)
							{
								while($row2 = mysql_fetch_array($res2))
								{	
									$receiptid = $row2['receipt_id'];
									$recid[] = $receiptid;
									
									$qry3 = "SELECT * FROM receipt_details where receipt_id='$receiptid'";												
									$res3 = mysql_query($qry3) or print(mysql_error());
									while($row3 = mysql_fetch_array($res3))
									{									
										$productid[] = $row3['product_id'];
										$quantity[] = $row3['quantity'];
										$rec_details_id[] = $row3['receipt_details_id'];														
									}												
								}											
						
								$recdetails = implode(",",$rec_details_id);
																			
								for($i=0;$i<10;$i++)
								{		
									$qry4 = "SELECT COUNT(product_id) as cnt,product_id,SUM(quantity) as quantity FROM receipt_details where product_id='$products[$i]' and receipt_details_id IN ($recdetails) ";												
									//echo "<br>";
									$res4 = mysql_query($qry4) or print(mysql_error());
									//echo mysql_num_rows($res4);
									$rows4 = mysql_fetch_array($res4);
																																			
									if($rows4['cnt'] != 0)											
									{														
										echo "<td align='right'>".$rows4['quantity']."</td>";	
										$totalProducts += $rows4['quantity'];
									}
									else
									{
										echo "<td align='right'>0</td>";
									}
								}
							}
							else
							{
								for($i=0;$i<10;$i++)
								{	
									echo "<td align='right'>0</td>";
								}
							}											
							echo "<td align='right'>".$totalProducts."</td>";
							echo "<td align='right'>".$row['win_product_id']."</td>";
							echo "<td align='right'>".$row['is_jackpot']."</td>";
							echo "<td align='right'>".$row['percent']."</td>";
						
							unset($recdetails);
							unset($rec_details_id);
							unset($productid);
							unset($quantity);
							$m++;										
						}
						else
						{
							for($i=0;$i<10;$i++)
							{	
								echo "<td align='right'>0</td>";
							}
						}
					}
					/*									
					echo "<pre>";print_r($recdetails);
					echo "<pre>";print_r($productid);												
					print_r($quantity);
					*/				
					
					?>
					</tbody>
					
				</table>
			</div><!-- /.box-body -->
	
	</div> 
</body>
</html>

